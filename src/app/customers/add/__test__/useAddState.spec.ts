import { act, renderHook } from '@testing-library/react-hooks'

import { useAddState } from '../useAddState'

describe('add.useAddState()', () => {
  it('should have default state', () => {
    const onAdd = jest.fn(() => ({}))
    const onAction = jest.fn(() => ({}))
    const { result } = renderHook(() => useAddState({ onAdd, onAction }))

    expect(result.current.name).toEqual('')
  })

  it('should change name', () => {
    const name = 'name'
    const onAdd = jest.fn(() => ({}))
    const onAction = jest.fn(() => ({}))
    const { result } = renderHook(() => useAddState({ onAdd, onAction }))

    act(() => result.current.onChange({ currentTarget: { value: name } } as React.ChangeEvent<HTMLInputElement>))

    expect(result.current.name).toEqual(name)
  })

  it('should fire on submit name', () => {
    const name = 'name'
    const onAdd = jest.fn(() => ({}))
    const onAction = jest.fn(() => ({}))
    const { result } = renderHook(() => useAddState({ onAdd, onAction }))

    act(() => result.current.onChange({ currentTarget: { value: name } } as React.ChangeEvent<HTMLInputElement>))
    act(() => result.current.onSubmit({ preventDefault: () => {} } as React.ChangeEvent<HTMLFormElement>))

    expect(result.current.name).toEqual('')
    expect(onAdd).toBeCalledWith(name)
    expect(onAction).toBeCalled()
  })
})