import React, { FunctionComponent } from 'react'

import { HeaderH1 } from '@/components/typography'

import { useCustomersRoute } from './useCustomersRoute'
import { useCustomersState } from './useCustomersState'

import { Layout, Title } from './styled'

import { Add } from './add'
import { Information } from './information'
import { List } from './list'

export const Customers: FunctionComponent = () => {
  const { customerId, navigateToCustomer, navigateToDefault } = useCustomersRoute()
  const {
    customers,
    selectedCustomer,
    addCustomer,
    addFeedback,
   } = useCustomersState({ customerId })

  return (
    <Layout isCustomerSelected={!!customerId}>
      <Title>
        <HeaderH1>Customer Feedback</HeaderH1>
      </Title>
      <List
        items={customers}
        selectedId={customerId}
        onSelect={navigateToCustomer}
      >
        <Add onAdd={addCustomer} />
      </List>
      <Information customer={selectedCustomer} onClose={navigateToDefault}>
        <Add onAdd={addFeedback} />
      </Information>
    </Layout>
  )
}
