import styled, { css } from '@/styled-components'

export const Layout = styled.div`
  ${({ theme: { spaces } }) => css`
    grid-area: information;

    padding: 0 ${spaces.x1};
    
    display: grid;
    grid-auto-flow: row;
    grid-gap: ${spaces.x2};

    grid-auto-rows: min-content;
  `}
`

export const Title = styled.div`
  ${({ theme: { spaces } }) => css`
    display: grid;
    grid-auto-flow: column;
    grid-gap: ${spaces.x2};

    grid-auto-rows: min-content;
    align-items: center;
  `}
`

export const CloseButton = styled.button``

export const LayoutEmpty = styled(Layout)`
  grid-template-rows: auto 1fr;
`

export const EmptyBlock = styled.div`
  display: grid;
  text-align: center;
  align-items: center;
`
