import { act, renderHook } from '@testing-library/react-hooks'

import { ICustomerModel } from '@/services/customers'

import { useCustomersState } from '../useCustomersState'

describe('customers.useCustomersState()', () => {
  it('should have default state', () => {
    const customerId = 0
    const { result } = renderHook(() => useCustomersState({ customerId }))

    expect(result.current.customers).toEqual([])
    expect(result.current.selectedCustomer).toEqual(undefined)
  })

  it('should add customer', () => {
    const customerId = 0
    const name = 'name'
    const { result } = renderHook(() => useCustomersState({ customerId }))

    act(() => result.current.addCustomer(name))

    const customer: ICustomerModel = {
      feedbacks: [],
      id: 1,
      name,
    }
    expect(result.current.customers).toMatchObject([customer])
    expect(result.current.selectedCustomer).toEqual(undefined)
  })

  it('should not add customer', () => {
    const customerId = 0
    const name = ''
    const { result } = renderHook(() => useCustomersState({ customerId }))

    act(() => result.current.addCustomer(name))

    expect(result.current.customers).toMatchObject([])
    expect(result.current.selectedCustomer).toEqual(undefined)
  })

  it('should select customer', () => {
    const customerId = 1
    const name = 'name'
    const { result } = renderHook(() => useCustomersState({ customerId }))

    act(() => result.current.addCustomer(name))

    const customer: ICustomerModel = {
      feedbacks: [],
      id: 1,
      name,
    }
    expect(result.current.customers).toMatchObject([customer])
    expect(result.current.selectedCustomer).toMatchObject(customer)
  })

  it('should add feedback', () => {
    const customerId = 1
    const name = 'name'
    const text = 'text'
    const { result } = renderHook(() => useCustomersState({ customerId }))

    act(() => result.current.addCustomer(name))
    act(() => result.current.addFeedback(text))

    const customer = {
      feedbacks: [{ id: 1, text }],
      id: 1,
      name,
    }
    expect(result.current.customers).toMatchObject([customer])
    expect(result.current.selectedCustomer).toMatchObject(customer)
  })

  it('should not add feedback with empty text', () => {
    const customerId = 1
    const name = 'name'
    const text = ''
    const { result } = renderHook(() => useCustomersState({ customerId }))

    act(() => result.current.addCustomer(name))
    act(() => result.current.addFeedback(text))

    const customer: ICustomerModel = {
      feedbacks: [],
      id: 1,
      name,
    }
    expect(result.current.customers).toMatchObject([customer])
    expect(result.current.selectedCustomer).toMatchObject(customer)
  })

  it('should not add feedback with empty selected customer', () => {
    const customerId: number = undefined
    const name = 'name'
    const text = 'text'
    const { result } = renderHook(() => useCustomersState({ customerId }))

    act(() => result.current.addCustomer(name))
    act(() => result.current.addFeedback(text))

    const customer: ICustomerModel = {
      feedbacks: [],
      id: 1,
      name,
    }
    expect(result.current.customers).toMatchObject([customer])
    expect(result.current.selectedCustomer).toEqual(undefined)
  })
})