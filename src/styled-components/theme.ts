interface IColors {
  itemBorder: string
  itemHover: string
  itemSelected: string
}

interface ISpaces {
  x1: string
  x2: string
}

interface IValues {
  itemBorder: string
  itemBorderRadius: string
}

export interface ITheme {
  spaces: ISpaces
  values: IValues
  colors: IColors
}

export const theme: ITheme = {
  colors: {
    itemBorder: '#d3d3d3',
    itemHover: '#d3d3d3',
    itemSelected: '#ededed',
  },
  spaces: {
    x1: '0.5rem',
    x2: '1rem',
  },
  values: {
    itemBorder: '1px solid',
    itemBorderRadius: '4px',
  }
}
