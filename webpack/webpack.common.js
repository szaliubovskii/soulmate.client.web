const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const Dotenv = require('dotenv-webpack')

const basePath = path.join(__dirname, '../')
const outPath = path.join(basePath, './build')
const sourcePath = path.join(basePath, './src')

module.exports = {
  context: sourcePath,
  entry: './index.tsx',
  output: {
    path: outPath,
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        use: 'ts-loader',
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        include: /node_modules/,
        test: /\.mjs$/,
        type: 'javascript/auto',
      },
      { test: /\.html$/, use: 'html-loader' },
    ],
  },
  optimization: {
    usedExports: true,
  },
  plugins: [
    new Dotenv(),
    new CleanWebpackPlugin(),
    new CaseSensitivePathsPlugin(),
    new HtmlWebpackPlugin({
      template: './index.html',
    }),
  ],
  resolve: {
    alias: {
      '@': path.resolve(sourcePath, './'),
      '@config': path.resolve(basePath, './config/'),
    },
    extensions: ['.mjs', '.js', '.ts', '.tsx', '.json'],
  },
}
