const dev = require('./webpack.dev')
const prod = require('./webpack.prod')

module.exports = {
  dev,
  prod,
}
