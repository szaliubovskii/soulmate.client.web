# Customer Feedback

## Description

### Intro

Goal of this challenge to see how you approach a task, how you structure the project and
how you can present the results.

### Technical Requirements

The challenge is intentionally not very specific on details to give you space for own decision.
To share our expectations we came up with these basic requirements:

- React
- No backend service required
- No need to store data changes persistently
- You can use a state management like Redux

Please commit your project to a git repository and share it with us once you're ready. Please
avoid to reference `name` in your code/description.

### Challenge

We want to build an interface that shows the feedback that we got from customers. By
default you can see the list of all customers who gave feedback. As soon as you click on one
of them you will see all their feedbacks in a list on the right side. You can add customers and
related feedbacks.

#### Initial View

When you open the page you will see a the list of customers on the left side. The right side is
empty.

#### Customer View

When you click on a customer, their feedback is showing up on the right side and the
customer is highlighted in the list.

#### Add Customer

When you click on "add new", a new entry with an input field will show up in the list. You can
enter the customer name and when you press the enter key the entry will be saved.

#### Add Feedback

For adding customer feedback the same logic applies as for adding a new customer.

## Personal challenge feedback

### UX

Application will provide all necessary information on one page, which is great. However, there could be potential issues with the `mobile first` approach. The UI is designed for desktop without taking into account mobile devices with less screen real estate.
Also there is a gap in functionality that UI doesn't take into account:

- edit mode (it would be in presentation mode of the whole list)
- delete (there could be a problem with clicking on a customer and clicking on delete in the same screen)
- pagination (it would be difficult to identify selected customer and track pagination at the same time)

I'll implement application as it's in the description with slight modifications, by adding additional elements for actions that would help mobile devices.

Screens in better to separate into two:

- customers list with number of feedbacks
- customer information with feedbacks (to which you can navigate from list of customers)

In case of domain changes (adding additional information for customer), we already will have a dedicated screen for customer with all needed information, that is editable.

## run

On how to run application.

### local

```bash
# install dependencies
npm ci

# run application
npm start
```

### docker

```bash
# using make
make local

# using docker compose
docker-compose up --build
```
